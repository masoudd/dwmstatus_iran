# dwmstatus_iran

fork of https://git.suckless.org/dwmstatus/ changed to show:

- Network {down,up}load speed
- Jalali calendar (using [jcal-git](https://aur.archlinux.org/packages/jcal-git))
- Available memory from /proc/meminfo
- CPU load from getloadavg() from stdlib.h

## Preview:
`(0.04) [2775MiB] [⬇0.00 KB/s¦⬆0.00 KB/s] December 2020-12-11 Jom 1399-09-21 18:42`
