#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <jalali.h>
#include <jtime.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Xlib.h>

char *tztehran = "Asia/Tehran";

static Display *dpy;

char *
smprintf(char *fmt, ...)
{
    va_list fmtargs;
    char *ret;
    int len;

    va_start(fmtargs, fmt);
    len = vsnprintf(NULL, 0, fmt, fmtargs);
    va_end(fmtargs);

    ret = malloc(++len);
    if (ret == NULL) {
        perror("malloc");
        exit(1);
    }

    va_start(fmtargs, fmt);
    vsnprintf(ret, len, fmt, fmtargs);
    va_end(fmtargs);

    return ret;
}

void
settz(char *tzname)
{
    setenv("TZ", tzname, 1);
}

int
parse_netdev(unsigned long long int *receivedabs, unsigned long long int *sentabs)
{
    char buf[255];
    char *datastart;
    static int bufsize;
    int rval;
    FILE *devfd;
    unsigned long long int receivedacc, sentacc;

    bufsize = 255;
    devfd = fopen("/proc/net/dev", "r");
    rval = 1;

    // Ignore the first two lines of the file
    fgets(buf, bufsize, devfd);
    fgets(buf, bufsize, devfd);

    while (fgets(buf, bufsize, devfd)) {
        if ((datastart = strstr(buf, "lo:")) == NULL) {
            datastart = strstr(buf, ":");

            // With thanks to the conky project at http://conky.sourceforge.net/
            sscanf(datastart + 1, "%llu  %*d     %*d  %*d  %*d  %*d   %*d        %*d       %llu",\
               &receivedacc, &sentacc);
            *receivedabs += receivedacc;
            *sentabs += sentacc;
            rval = 0;
        }
    }

    fclose(devfd);
    return rval;
}

void
calculate_speed(char *speedstr, unsigned long long int newval, unsigned long long int oldval)
{
    double speed;
    speed = (newval - oldval) / 1024.0;
    if (speed > 1024.0) {
        speed /= 1024.0;
        sprintf(speedstr, "%.3f MB/s", speed);
    } else {
        sprintf(speedstr, "%.2f KB/s", speed);
    }
}

char *
get_netusage(unsigned long long int *rec, unsigned long long int *sent)
{
    unsigned long long int newrec, newsent;
    newrec = newsent = 0;
    char downspeedstr[15], upspeedstr[15];
    static char retstr[42];
    int retval;

    retval = parse_netdev(&newrec, &newsent);
    if (retval) {
        fprintf(stdout, "Error when parsing /proc/net/dev file.\n");
        exit(1);
    }

    calculate_speed(downspeedstr, newrec, *rec);
    calculate_speed(upspeedstr, newsent, *sent);

    sprintf(retstr, "⬇%s¦⬆%s", downspeedstr, upspeedstr);

    *rec = newrec;
    *sent = newsent;
    return retstr;
}

char *
mktimes(char *fmt, char *tzname)
{
    char buf[129];
    time_t tim;
    struct tm *timtm;

    bzero(buf, sizeof(buf));
    settz(tzname);
    tim = time(NULL);
    timtm = localtime(&tim);
    if (timtm == NULL) {
        perror("localtime");
        exit(1);
    }

    if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
        fprintf(stderr, "strftime == 0\n");
        exit(1);
    }

    return smprintf("%s", buf);
}

char *
jmktimes(char *fmt, char *tzname)
{
    char buf[129];
    time_t tim;
    struct jtm *timtm;

    bzero(buf, sizeof(buf));
    settz(tzname);
    tim = time(NULL);
    timtm = jlocaltime(&tim);
    if (timtm == NULL) {
        perror("localtime");
        exit(1);
    }

    if (!jstrftime(buf, sizeof(buf)-1, fmt, timtm)) {
        fprintf(stderr, "strftime == 0\n");
        exit(1);
    }

    return smprintf("%s", buf);
}

void
setstatus(char *str)
{
    XStoreName(dpy, DefaultRootWindow(dpy), str);
    XSync(dpy, False);
}

char *
loadavg(void)
{
    double avg;

    if (getloadavg(&avg, 1) < 0) {
        perror("getloadavg");
        exit(1);
    }

    return smprintf("%.2f", avg);
}

// from htop StringUtils.c
#define String_startsWith(s, match) (strncmp((s),(match),strlen(match)) == 0)

char *
get_memavail(void)
{
    unsigned long long int MemAvailable = 0;

    FILE *file = fopen("/proc/meminfo", "r");
    char buffer[128];
    if (file == NULL) {
        perror("ERR memavail");
        exit(1);
    }

    while (fgets(buffer, 128, file)) {
        if (buffer[0] == 'M' && String_startsWith(buffer, "MemAvailable")) {
            int ret = sscanf(buffer + strlen("MemAvailable:"), " %32llu kB", &MemAvailable);
            if (ret != 1) {
                perror("ERR memavail");
                exit(1);
            }
        }
    }

    fclose(file);
    MemAvailable /= 1024; //to MiB
    return smprintf("%lluMiB", MemAvailable);
}

int
main(void)
{
    char *status;
    char *avgs;
    char *memavail;
    char *netstats;
    char *tmtehran;
    char *jtmtehran;
    static unsigned long long int rec, sent;

    if (!(dpy = XOpenDisplay(NULL))) {
        fprintf(stderr, "dwmstatus_iran: cannot open display.\n");
        return 1;
    }

    parse_netdev(&rec, &sent);
    for (;;sleep(1)) {
        avgs = loadavg();
        memavail = get_memavail();
        netstats = get_netusage(&rec, &sent);
        tmtehran = mktimes("%B %F", tztehran);
        jtmtehran = jmktimes("%h %F %R", tztehran);


        status = smprintf("(%s) [%s] [%s] %s %s",
                          avgs, memavail, netstats, tmtehran, jtmtehran);
        setstatus(status);

        //for debugging:
        //printf("%s\n", status);

        free(avgs);
        free(memavail);
        free(tmtehran);
        free(jtmtehran);
        free(status);
    }

    XCloseDisplay(dpy);

    return 0;
}

