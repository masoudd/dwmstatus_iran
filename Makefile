dwmstatus_iran: dwmstatus_iran.c
	gcc -std=c99 -pedantic -Wall -O2 -lX11 -ljalali -I/usr/include/jalali dwmstatus_iran.c -o dwmstatus_iran

clean:
	rm -f dwmstatus_iran

.PHONY: all options clean dist install uninstall
